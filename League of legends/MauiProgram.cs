﻿using Microsoft.Extensions.Logging;
using Model;
using StubLib;
using VeiwModel;

namespace League_of_legends;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");

			})
			.Services
			.AddSingleton<IDataManager,StubData>()
			.AddSingleton<ManagerVM>()
			.AddSingleton<MainPage>()
			;


#if DEBUG
		builder.Logging.AddDebug();
#endif

		return builder.Build();
	}
}

