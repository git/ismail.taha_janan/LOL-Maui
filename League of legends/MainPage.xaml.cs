﻿namespace League_of_legends;


using League_of_legends.ViewModel;
using Model;

public partial class MainPage : ContentPage
{

	private readonly MainVM mainVM = new();

	

	public MainPage()
    {
		InitializeComponent();
		BindingContext = mainVM;
	}


}


