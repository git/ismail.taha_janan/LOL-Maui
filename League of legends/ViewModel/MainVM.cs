﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Model;
using VeiwModel;

namespace League_of_legends.ViewModel
{
	public class MainVM:INotifyPropertyChanged
	{
        //private readonly IDataManager dataManager;

        public event PropertyChangedEventHandler PropertyChanged;

        void OnPropsChanged(string name) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        //public ReadOnlyObservableCollection<ChapionVM> ChapionVMs { get; private set; }
        //private readonly ObservableCollection<ChapionVM> chapionVMs = new();



        //private ManagerVM managerVM = new(new ChapionVM());

        private string text;

        public string Text
        {
            get => text;
            set
            {
                text = value;
                OnPropsChanged(nameof(Text));
            }
        }

        public MainVM()
        {
            //ChapionVMs = new ReadOnlyObservableCollection<ChapionVM>(chapionVMs);
            //loadChamps();

        }

        //private async void loadChamps()
        //{
        //    IEnumerable<Champion> champs = await dataManager.ChampionsMgr.GetItems(1,5);
        //    foreach (Champion champ in champs)
        //    {
        //        chapionVMs.Add(new ChapionVM(champ));
        //    }
        //}


    }
}

