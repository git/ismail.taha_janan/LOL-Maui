# League of Legends Information Application

The League of Legends Information Application is a comprehensive mobile application designed to provide players with valuable information and resources about the popular online multiplayer game, League of Legends. Whether you are a seasoned veteran or a beginner, this app aims to enhance your gaming experience by offering a wide range of features and functionalities.

## Features

### Champion Database
The application includes an extensive champion database that contains detailed information about each playable champion in League of Legends. Users can explore champions' abilities, stats, lore, recommended items, and even watch videos showcasing their gameplay.


### Item Build Guides
The application offers item build guides for different champions, presenting recommended item builds, explanations, and situational suggestions. Users can easily access popular builds and experiment with different item combinations to optimize their strategies.

### News and Updates
1. :white_check_mark:List of champions
2. :white_check_mark:Pagination
3. :white_check_mark:show Details of a champion
4. :white_check_mark:delete a champion in swipe left
5. :x:start making update but didnt have time to finish it(you will find what i have to do in the button's function)
