﻿using System;
using System.ComponentModel;
using Model;

namespace VeiwModel
{
	public class SkinVM: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        void OnPropsChanged(string name) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        public string Name
        {
            get => name; set
            {
                name = value;
                OnPropsChanged(nameof(Name));
            }
        }
        private string name;
        public string Icon { get; set; }
        public float Price { get; set; }
        public string PriceToString { get {
                string ptice = Price.ToString() + " $";
                return ptice;
            }set { } }


        public SkinVM(Skin skin)
		{
			this.Name = skin.Name;
			this.Icon = skin.Icon;
			this.Price = skin.Price;
		}

        
    }
}

