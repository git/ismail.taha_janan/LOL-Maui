﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Model;
namespace VeiwModel
{
	public class ChapionVM: INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;
        void OnPropsChanged(string name) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));

        public string Name { get =>name  ; set
            {
                name = value;
                OnPropsChanged(nameof(Name));
            } }
        private string name;

		public ChampionClass Class { get; private set; }
		public string Icon { get; private set; }
		public LargeImage Image { get; private set; }
		public string Bio { get;  set; }

        //public ReadOnlyDictionary<string, int> Characteristics { get; private set; }
        //private readonly Dictionary<string, int> characteristics = new Dictionary<string, int>();

        public ReadOnlyObservableCollection<SkinVM> SkinVMs { get ; private set; }

        private ObservableCollection<SkinVM> skinVMs = new();

        public static Champion championVMTOChampion(ChapionVM chapionVM)
        {
            string? image = chapionVM.Image.ToString();
            Champion champion = new Champion(
                chapionVM.Name,
                chapionVM.Class,
                chapionVM.Icon,
                image: image,
                chapionVM.Bio
                );
            return champion;
        }

        public ChapionVM(Champion champ)
		{
            Name = champ.Name;
            Class = champ.Class;
            Icon = champ.Icon;
            Image = champ.Image;
            Bio = champ.Bio;
            SkinVMs = new ReadOnlyObservableCollection<SkinVM>(skinVMs);
            //Characteristics = champ.Characteristics;
            foreach (var skin in champ.Skins){
                this.skinVMs.Add(new SkinVM(skin));
            }
        }

        public ChapionVM() { }

        


        
    }
}

