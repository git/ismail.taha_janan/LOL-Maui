﻿using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using Microsoft.Maui.Controls;
using Model;
using StubLib;

namespace VeiwModel;
public class ManagerVM : INotifyPropertyChanged
{


    public event PropertyChangedEventHandler? PropertyChanged;
    void OnPropsChanged(string name) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));


    public ReadOnlyObservableCollection<ChapionVM> ChapionVMs { get; private set; }
    private readonly ObservableCollection<ChapionVM> chapionVMs = new();

    
    private ChapionVM? selctedChampVM = new();
    public ChapionVM? SelctedChampVM { get => selctedChampVM; set {
            selctedChampVM = value;
            OnPropsChanged(nameof(SelctedChampVM));
        } }


    private readonly IDataManager dataManager;

    public ManagerVM(IDataManager dataManager)
    {
        this.dataManager = dataManager;
        PrevPage = new Command(PreviousPage);
        NextPage = new Command(_NextPage);
        DeleteChamp = new Command<ChapionVM>(deleteChamp);
        ChapionVMs = new ReadOnlyObservableCollection<ChapionVM>(chapionVMs);
        IndexPage = 1;

        //this.dataManager = dataManager;
        LoadChamps();
        LoadChampsNumb();

    }

    public ICommand PrevPage { get; }
    public ICommand NextPage { get; }
    public ICommand DeleteChamp { get; }



    private int indexPage = 1 ;
    public int countPerPage = 5;
    private int champsNumber;


    public int IndexPage
    {
        get => indexPage;
        set
        {
            indexPage = value;
            OnPropsChanged(nameof(IndexPage));
        }
    }

    public int ChampsNumber
    {
        get => champsNumber;
        set
        {
            champsNumber = value;
            OnPropsChanged(nameof(ChampsNumber));
        }
    }






    private async void PreviousPage()
    {

        

        if (indexPage > 1)
        {
            IndexPage = IndexPage - 1;
        }
        chapionVMs.Clear();
        await LoadChamps();
    }
    private async void _NextPage()
    {

        

        if (indexPage<=(int) Math.Ceiling((double) champsNumber/countPerPage)-1)
        {
            IndexPage = IndexPage + 1;
            chapionVMs.Clear();
            await LoadChamps();
        }
        
        
    }

    

    private async Task LoadChampsNumb()
    {
        champsNumber = await dataManager.ChampionsMgr.GetNbItems();
    }

    private async void deleteChamp(ChapionVM chapionVM)
    {
        bool x = await dataManager.ChampionsMgr.DeleteItem(ChapionVM.championVMTOChampion(chapionVM));
        if (x) {
            chapionVMs.Remove(chapionVM);
            
        }
    }

    private async Task LoadChamps()
    {
        IEnumerable<Champion> champs = await dataManager.ChampionsMgr.GetItems(indexPage - 1, countPerPage);

        foreach(var champ in champs)
        {
            Console.WriteLine(champ.Name);
            chapionVMs.Add(new ChapionVM(champ));
        }
    }

}

