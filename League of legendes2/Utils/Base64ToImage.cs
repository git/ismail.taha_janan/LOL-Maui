﻿using System;
using System.Globalization;

namespace League_of_legendes2.Utils
{
	public class Base64ToImage:IValueConverter
	{
		public Base64ToImage()
		{
		}

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string input = value as string;
            byte[] decodedImg = System.Convert.FromBase64String(input);
            Stream memoryStream = new MemoryStream(decodedImg);
            ImageSource output = ImageSource.FromStream(() => memoryStream);
            return output;

        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}

