﻿using Model;
using VeiwModel;

namespace League_of_legendes2.Pages;

public partial class ChampForm : ContentPage
{

	ManagerVM managerVM;
	public ChapionVM championVM { get; set; }
    ChapionVM OldChapionVM;



    public ChampForm(ChapionVM chapionVM)
    {
        this.championVM = chapionVM;
        OldChapionVM = chapionVM;
        InitializeComponent();
        BindingContext = this;
    }

    void Button_Clicked(System.Object sender, System.EventArgs e)
    {
        // I want to rn a command that have to exist in ChampManager to update it
        // ill need to pass old and new champ and creat a mapper from chamVM to Champ so i can use data manager functions
        
    }

    void Editor_TextChanged(System.Object sender, Microsoft.Maui.Controls.TextChangedEventArgs e) => championVM.Bio = e.NewTextValue;
}
