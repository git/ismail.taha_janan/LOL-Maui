﻿using League_of_legendes2.ViewModel;
using VeiwModel;

namespace League_of_legendes2.Pages;

public partial class Details : ContentPage
{

	public ChapionVM chapionVM { get; set; }

	

	



	public Details(ChapionVM chapion)
    {
        chapionVM = chapion;
        InitializeComponent();
		BindingContext = this;
	}

    public Details()
    {
    }

    //public async void goTOUpdate()
    //{
    //    await Navigation.PushAsync(new Pages.ChampForm());
    //}

    async void ToolbarItem_Clicked(System.Object sender, System.EventArgs e)
    {
        await Navigation.PushAsync(new Pages.ChampForm(chapionVM));
    }

    //void ToolbarItem_Clicked(System.Object sender, System.EventArgs e)
    //{
    //}
}
