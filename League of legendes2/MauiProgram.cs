﻿using League_of_legendes2.ViewModel;
using Microsoft.Extensions.Logging;
using Model;
using StubLib;
using VeiwModel;

namespace League_of_legendes2;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
			}).Services
			.AddSingleton<IDataManager, StubData>()
            .AddSingleton<ManagerVM>()
            .AddSingleton<MainVM>()
            .AddSingleton<MainPage>();

#if DEBUG
		builder.Logging.AddDebug();
#endif

		return builder.Build();
	}
}

